<?php

$upload_dir = "img";

$errors = false;
$errFields = array();
$errMessages = array();

/*echo "POST är: <br><pre>";
var_dump($_POST);
echo "<br></pre>FILE är: <br><pre>";
var_dump($_FILES);

echo "<br></pre>fieldsToValidate är: <br><pre>";
var_dump($fieldsToValidate);*/


$reqFields = array('namn', 'nr', 'pris', 'color');

$productInfo = array();


/**
** Lägger till all information som ska läggas till i produkttabellen samt vilka färger den finns som i en array. /LINDA
**/
foreach ($reqFields as $field) {
	if (array_key_exists($field, $_POST)) {
		$productInfo[$field] = $_POST[$field];
	}	
}

if ($_FILES['bild']['name']) {
	$error = false;
	$productInfo['bild'] = $_FILES['bild'];
	if ($_FILES['bild']['size'] > 1024000) {
		$error = true;
		$errFields[] = 'bild';
				
	} elseif ($_FILES['bild']['size'] < 1) {
		$error = true;
		$errFields[] = 'bild';
	} 
}

/**
* Validerar enligt regler som är satta i fields_to_validate.php. //Linda
**/
foreach ($fieldsToValidate as $field => $methods) {
	
	foreach ($methods as $method) {
		
		if (array_key_exists($field, $productInfo)) {
			

			if ($method === 'stringFunctions') {
					$productInfo[$field] = $method($productInfo[$field]);
					
			
			} else if ($method($productInfo[$field])){
				
				$errFields[] = $field;
				$errors = true;
				/**
				* Sparar ett felmeddelande beroende på vilken metod som används och vilket fält som validerats. //Linda
				**/
				$errMessages[] = $errorLines[$method] . $errorNames[$field];
			}
		}
	}
}
/*echo "<br></pre>productInfo är: <br><pre>";
var_dump($productInfo);*/

/**
** Kontrollerar så att angivet serienummer inte redan finns i produkttabellen. /LINDA
**/
$sql = "SELECT serie_nr FROM produkter WHERE serie_nr = :nr";
	$stmt = $pdo->prepare($sql);
	$stmt->bindParam(':nr', $productInfo['nr']);
	$stmt->execute();

	if ( $stmt->fetchColumn() ) {

		$errMessages[] = "Serienumret finns redan i databasen!";
		$errors = true;

	}

if (!$errors) {

/**
** Lägger till all information i produkttabellen. /LINDA
**/
		$sql = "INSERT INTO produkter (namn, serie_nr, pris, img) VALUES (:namn, :nr, :pris, :img)";
				
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':namn', $productInfo['namn']);
		$stmt->bindParam(':nr', $productInfo['nr']);
		$stmt->bindParam(':pris', $productInfo['pris']);
		$stmt->bindParam(':img', $productInfo['bild']['name']);
	

		$stmt->execute();

		move_uploaded_file($productInfo['bild']['tmp_name'], $upload_dir."/".$productInfo['bild']['name']);

/**
** Lägger till all information i colorstabellen. / LINDA
**/
	$lastPid = $pdo->lastInsertId();

		foreach ($productInfo['color'] as $color) {
			
			$sql2 = "INSERT INTO color_prod (prod_id, color_id) VALUES (:pid, :color_id)";

			$stmt2 = $pdo->prepare($sql2);
			$stmt2->bindParam(':pid', $lastPid);
			$stmt2->bindParam(':color_id', $color);

			$stmt2->execute();
		}
	include_once('action/all.products.act.php');

} else {
	include_once('action/form.prod.act.php');
}

/*echo "<br>productInfo är: <br>";
var_dump($productInfo);

echo "<br>Errorfält är: <br>";
var_dump($errFields);*/