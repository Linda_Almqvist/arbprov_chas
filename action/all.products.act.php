<?php 


$sql = "SELECT pid as pid, namn as namn, serie_nr as nr, pris as pris, img as img, updated_time as datum FROM produkter";

	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	$produktData = $stmt->fetchAll(PDO::FETCH_OBJ);

$sqlColors = "SELECT colors.idcolors as id, colors.code as kod, colors.name as namn, produkter.pid as pid, color_prod.prod_id as cpid FROM colors INNER JOIN color_prod ON colors.idcolors = color_prod.color_id INNER JOIN produkter ON produkter.pid = color_prod.prod_id";


	$stmtColors = $pdo->prepare($sqlColors);
	$stmtColors->execute();
	$colorProdData = $stmtColors->fetchAll(PDO::FETCH_OBJ);

	


if ($produktData) {
	include("tpl/all.products.tpl.php");
}