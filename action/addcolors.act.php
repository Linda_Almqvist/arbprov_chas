<?php


$errors = false;
$errFields = array();
$errMessages = array();



$reqFields = array('namn', 'kod');

$colorInfo = array();


foreach ($reqFields as $field) {
	if (array_key_exists($field, $_POST)) {
		$colorInfo[$field] = $_POST[$field];
	}	
}

/**
* Validerar enligt regler som är satta i fields_to_validate.php. //Linda
**/
foreach ($fieldsToValidate as $field => $methods) {
	
	foreach ($methods as $method) {
		
		if (array_key_exists($field, $colorInfo)) {
			

			if ($method === 'stringFunctions') {
					$colorInfo[$field] = $method($colorInfo[$field]);
					
			
			} else if ($method($colorInfo[$field])){
				
				$errFields[] = $field;
				$errors = true;
				/**
				* Sparar ett felmeddelande beroende på vilken metod som används och vilket fält som validerats. //Linda
				**/
				$errMessages[] = $errorLines[$method] . $errorNames[$field];
			}
		}
	}
}


if (!$errors) {

/**
** Lägger till all information i produkttabellen. /LINDA
**/
		$sql = "INSERT INTO colors (name, code) VALUES (:namn, :kod)";
				
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(':namn', $colorInfo['namn']);
		$stmt->bindParam(':kod', $colorInfo['kod']);

		$stmt->execute();



	include_once('action/all.colors.act.php');

} else {
	include_once('action/form.colors.act.php');
}

