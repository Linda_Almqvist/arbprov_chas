<?php
/*
** Array med alla fält som ska valideras och vilka funktioner de ska valideras i. //Linda
*/
$fieldsToValidate = array(
	'namn' => array('isEmpty', 'stringFunctions'), 
	'nr' => array('isEmpty', 'notOnlyNumbers', 'stringFunctions'), 
	'pris' => array('isEmpty', 'notOnlyNumbers','stringFunctions'),   
	'kod' => array('stringFunctions'),   
	);
