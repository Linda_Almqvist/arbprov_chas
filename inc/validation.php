<?php

/**
* Validerar så att fältet är ifyllt. /Linda
**/
function isEmpty($field) {
	//echo $field." Valideras i ISEMPTY<br>";
	if ($field === "") {
		return true;
	}
	return false;
}


/**
* Validerar och filtrerar stängar, tar bort skadlig kod såsom tags. /Linda
**/
function stringFunctions($field) {
		//echo $field." Valideras i STRING<br>";
	$field = trim($field);
	$field = strip_tags($field);
	$field = htmlspecialchars($field);
//echo $field." Validerad i STRING<br>";
	return $field;
}


/**
* Validerar så att fältet enbart innehåller siffror. /Linda
**/
function notOnlyNumbers($field) {
//echo $field." Valideras i numbers<br>";
	if (is_numeric($field)) {
		return false;
	} else {
		return true;
	}
}



