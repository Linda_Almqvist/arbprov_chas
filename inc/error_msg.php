<?php
/**
* Fördefinierade meddelanden för att bygga upp felmeddelanden beroende på fält och valideringsfunktion. /LINDA
**/
$errorLines = array(
	'isEmpty' => 'Du måste fylla i ett ', 
	'notOnlyNumbers' => 'Du kan bara fylla i nummer under ',  
);

$errorNames = array(
	'namn' => 'namn',
	'nr' => 'serienummer',
	'pris' => 'pris', 
);

