

<div class="wrapper_rad">
	<div class="color"><h3>Färg</h3></div>
	<div class="namn"><h3>Färgnamn</h3></div>
	<div class="kod"><h3>Hexkod</h3></div>
	<div class="id"><h3>Id</h3></div>
	
</div>

<?php 

foreach ($colorData as $color) {


	print('<div class="wrapper_rad">');
	printf('<div class="color" style="background-color: #%s; padding: 5px;"></div>', $color->code);
	printf('<div class="namn"><p>%s</p></div>', $color->name);
	printf('<div class="hex"><p>%s</p></div>', $color->code);
	printf('<div class="id"><p>%s</p></div>', $color->idcolors);

	print('</div>');

}

?>
<h4><a href="?action=start">Tillbaka till startsidan.</a></h4>
