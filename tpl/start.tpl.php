<style type="text/css">
	
div.start_div {
	width: 25%;
	text-align: center;
}

div.start_wrapper {
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    align-items: flex-start;
    margin-bottom: 50px;
   }

</style>
<div class="start_wrapper">

<div class="start_div">
	<a href="?action=form.prod.act">
		<h2 class="icon">p</h2>
		Lägg till produkt</a>
</div>

<div class="start_div">
	<a href="?action=form.colors.act">
		<h2 class="icon">f</h2>
		Lägg till färg</a>
</div>

<div class="start_div">
	<a href="?action=all.products.act">
		<h2 class="icon">i</h2>
		Visa alla produkter</a>
</div>
<div class="start_div">
	<a href="?action=all.colors.act">
		<h2 class="icon">c</h2>
		Visa alla färger</a>
</div>

</div>