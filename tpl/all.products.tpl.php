

<div class="wrapper_rad">
	<div class="bild"><h3>Bild</h3></div>
	<div class="namn"><h3>Produktnamn</h3></div>
	<div class="serie_nr"><h3>Serienr.</h3></div>
	<div class="pris"><h3>Pris</h3></div>
	<div class="datum"><h3>Färger</h3></div>
</div>

<?php 



foreach ($produktData as $produkt) {


	print('<div class="wrapper_rad">');
	printf('<div class="bild"><img src="img/%s"></div>', $produkt->img);
	printf('<div class="namn"><p>%s</p></div>', $produkt->namn);
	printf('<div class="serie_nr"><p>%s</p></div>', $produkt->nr);
	printf('<div class="pris"><p>%s</p></div>', $produkt->pris);
	print('<div class="colors">');

	foreach ($colorProdData as $color) {
		
		if ($color->pid === $produkt->pid) {
		printf('<span class="color_icon" style="color:#%s;"> <abbr title="%s">s</abbr> </span>', $color->kod, $color->namn);
		}
	}
	print('</div>');
	print('</div>');

}

?>

<h4><a href="?action=start">Tillbaka till startsidan.</a></h4>
