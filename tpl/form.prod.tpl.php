<style type="text/css">
	


</style>

<?php
	if (isset($errMessages)) {

		foreach ($errMessages as $key => $value) {
			printf('<p class="error_msg">%s</p>', $errMessages[$key]);
		}
		
	} 

?>
<h1>Lägg till ny produkt</h1>




<form class="form_prod" method="post" action="?action=addprod.act" enctype="multipart/form-data">
<div class="wrapper_top">
		<label for="namn">Produktnamn:</label>
		<input type="text" name="namn" id="namn">

		<label for="nr">Serienummer:</label>
		<input type="text" name="nr" id="nr">

		<label for="pris">Pris:</label>
		<input type="text" name="pris" id="pris">

		<label for="bild">Välj en bild: <span></span></label>
		<input type="file" name="bild" value="" id="bild">

</div>
<div class="wrapper_low">
	<label for="select_colors">Välj färger:</label>


<div class="category_section">
	<?php

//För att skriva ut färger på ett valbart sätt.

	foreach ($colorsData as $color) { 



		
		print('<fieldset class="chose_color">');
		printf('<input type="checkbox" name="color[]" class="cat_checkbox" id="color_%s" value="%s" %s>', $color->idcolors, $color->idcolors, (array_key_exists($color->idcolors, $currColors) ? 'checked' : ''));
		printf('<label class="label_color" for="color_%s">', $color->idcolors);
		printf('<h3><div class="color_prev" style="background-color: #%s;"></div>', $color->code);
		printf('%s</h3>', $color->name);
		print('</label></fieldset>');

	 } ?>

	</div>


</div>
	<button type="submit">Spara</button>

</form>

<h4><a href="?action=start">Tillbaka till startsidan.</a></h4>

