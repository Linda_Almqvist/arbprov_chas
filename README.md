SYSTEMKRAV
För att kunna köra sidan behöver du ha följande installerat på din dator. Apache Server, MySQL Server och PHP. 


DATABAS
Databasen heter db_prov och i den finns det totalt 3 st tabeller. Importera hela databasen till din lokala server genom att använda databasdumpen i mappen "Databas_dump".

SYSTEMET
Därefter behöver du spara ner hela sidan med samtliga dokument och mappar i din servers rootmapp och öppna via din lokala server i en webbläsare. Öppnar du mappen med dokumenten i din webbläsare så kommer filen index.php automatiskt att väljas. Det är genom den som hela sidan visas. För att kunna skapa en connection till databasen bör du gå in i dokumentet settings.php som ligger i inc-mappen. Där kan du kontrollera så att korrekta inställningar för din databas, server, användare och lösenord är ifyllda.


