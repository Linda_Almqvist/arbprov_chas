<?php
require_once ('inc/settings.php');

require_once('inc/fields_to_validate.php');
require_once('inc/error_msg.php');
require_once('inc/validation.php');
include('inc/head-data_inc.php');

	

	if (isset($_GET['action'])) {
		if (is_file('action/'.$_GET['action'].'.php')) {
			require('action/'.$_GET['action'].'.php');
		} else {
			echo "Sidan finns inte :(";
		}
	} else {
		require('action/start.php');
		
	}

include('inc/footer_tpl.php');